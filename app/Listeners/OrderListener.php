<?php

namespace App\Listeners;

use App\User;
use App\Events\OrderMaterials;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderMaterials  $event
     * @return void
     */
    public function handle(OrderMaterials $event)
    {
        // Mail::to($request->user())->send(new OrderShipped($order));
        // Mail::view('supply.order');
        // Mail::to(User::findById(1)->send($event->materials));
        Mail::send('supply-order', ['materials' => $event->materials], function ($message)
        {
            $message->from('me@gmail.com', 'Christian Nwamba');
            $message->to('chrisn@scotch.io');
        });

    }
}
