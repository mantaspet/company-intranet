<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentEmploymentContract extends Model {
    use SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "date",
        "document_id",
        "registration_number",
        "employer",
        "employer_code",
        "employer_representative",
        "employer_representative_nin",
        "employee_first_name",
        "employee_last_name",
        "employee_identity_card_number",
        "employee_identity_card_issuance_date",
        "employee_nin",
        "employee_address",
        "employee_position",
        "employee_salary",
        "employee_salary_comment",
        "valid_from",
        "started_from"
    ];

    // Relations
    // --------------------------------------------------------------

    public function document() {
        return $this->belongsTo('App\Document', 'document_id', 'id');
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "date" => "required|date",
            "registration_number" => "required",
            "employer" => "required",
            "employer_code" => "required",
            "employer_representative" => "required",
            "employer_representative_nin" => "required",
            "employee_first_name" => "required",
            "employee_last_name" => "required",
            "employee_identity_card_number" => "required",
            "employee_identity_card_issuance_date" => "required",
            "employee_nin" => "required",
            "employee_address" => "required",
            "employee_position" => "required",
            "employee_salary" => "required|numeric",
            "valid_from" => "required|date",
            "started_from" => "required|date"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
