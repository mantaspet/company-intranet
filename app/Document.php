<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model {
    use SoftDeletes;

    const DOCUMENT_TYPES = [
        'salary_transaction_request',
        'nti_application_request',
        'employment_contract'
    ];

    const AVAILABILITY_TYPES = [
        'private',
        'department_only',
        'public'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "title",
        "related_document_type",
        "related_document_id",
        "availability_type",
        "employee_id"
    ];

    // Relations
    // --------------------------------------------------------------

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function salaryTransactionRequest() {
        return $this->hasOne('App\DocumentSalaryTransactionRequest');
    }

    public function ntiApplicationRequest() {
        return $this->hasOne('App\DocumentNtiApplicationRequest');
    }

    public function employmentContract() {
        return $this->hasOne('App\DocumentEmploymentContract');
    }

    // --------------------------------------------------------------

    public function scopeWithDocumentContent($query) {
        return $query->with(['salaryTransactionRequest', 'ntiApplicationRequest', 'employmentContract']);
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::created(function($table) {
            $hash = 'oreo'.$table->id.$table->created_by;
            $table->url = Crypt::encrypt($hash);
            $table->save();
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "title" => "required",
            "url" => "required",
            "document_type" => "required|in:".implode(',', self::DOCUMENT_TYPES),
            "availability_type" => "required".implode(',', self::AVAILABILITY_TYPES),
            "employee_id" => "required|integer"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
