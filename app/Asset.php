<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model {
    use SoftDeletes;

    const AVAILABLE_TYPES = [
        'materialinis',
        'intelektinis'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "name",
        "value",
        "acquisition_date",
        "is_assigned",
        "type"
    ];

    protected $casts = [
        "is_assigned" => "boolean"
    ];

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "name" => "required",
            "type" => "required|in:".implode(',', self::AVAILABLE_TYPES),
        ];

        $validation = Validator::make($input = $data, $rules);

        //$validation->sometimes('attribute', 'not_exists:assets,title', function ($input) {
        //    return Auth::user()->role=='admin';
        //});

        return $validation;
    }

}
