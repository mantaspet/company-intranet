<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeesStore extends FormRequest
{
    const AVAILABLE_GENDERS = [
        'm',
        'f',
        'o'
    ];

    const AVAILABLE_STATUSES = [
        'dirba',
        'atostogauja',
        'nebedirba'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "department_id" => "required",
            "office_id" => "required",
            "position_id" => "required",
            "salary" => "required|numeric",

            "contact.nin" => "required",
            "contact.first_name" => "required",
            "contact.last_name" => "required",
            "contact.dob" => "required|date",
            "contact.email" => "required|email",
            "contact.phone" => "required",
            "contact.status" => "required|in:".implode(',', self::AVAILABLE_STATUSES),
            "contact.gender" => "required|in:".implode(',', self::AVAILABLE_GENDERS),
            "contact.country" => "required",
            "contact.city" => "required",
            "contact.address" => "required"
        ];
    }
}
