<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\EmployeeSalaryHistory;
use Illuminate\Support\Facades\Auth;
use App\DocumentEmployeeSalaryHistory;

class EmployeeSalaryHistoryController extends Controller
{

    public function index()
    {
        $query = EmployeeSalaryHistory::query();
        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getMySalaryHistory() {
        $user_id = (Auth::check()) ? Auth::user()->id : 1;
        $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        $query = EmployeeSalaryHistory::where('employee_id', $employee_id)->get();
        return response()->json(['item' => $query]);
    }

    public function show($id){
        $item = EmployeeSalaryHistory::where('employee_id', $id)->get();
        // $this->authorize('view', $item);
        return response()->json(["item"=>$item]);
    }

    public function store(Request $request)
    {
        // $this->authorize('create', EmployeeSalaryHistory::class);

        $validation = EmployeeSalaryHistory::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $item = EmployeeSalaryHistory::create($request->all());
            return response()->json(["item"=>$item]);
        }
    }


    public function update(Request $request, $id)
    {
        $item = EmployeeSalaryHistory::findOrFail($id);

        // $this->authorize('update', $item);

        $validation = EmployeeSalaryHistory::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $item->update($request->all());
            return response()->json(["item"=>$item]);
        }
    }


    public function destroy($id)
    {
        $item = EmployeeSalaryHistory::findOrFail($id);

        // $this->authorize('delete', $item);

        $item->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?EmployeeSalaryHistory::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
