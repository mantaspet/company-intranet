<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use App\Employee;
use App\Position;
use App\HolidayHistory;
use App\EmployeeHistory;
use Illuminate\Http\Request;
use App\EmployeeSalaryHistory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EmployeeData;
use App\Http\Requests\EmployeesStore;

class EmployeesController extends Controller
{


    public function index()
    {
        $query = Employee::employeeInfoRelations();

        // $this->authorize('viewAll', Employee::class);

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id)
    {
        $query = Employee::employeeInfoRelations()->findOrFail($id);

        // !isset($query->employee_history)
        if ($query->employee_history == []) {
            
        }

        // $this->authorize('view', $query);

        return response()->json(["item"=>$query]);
    }


    public function store(EmployeesStore $request)
    {
        try {
            // $this->authorize('create', User::class);

            DB::beginTransaction();

            $contact = $request->get('contact');
            $contact = Contact::create($contact);

            $employee = Employee::create(
                array_merge($request->only(['office_id', 'department_id']), ['contact_id'=>$contact->id])
            );

            EmployeeHistory::create([
                'employee_id'   => $employee->id, 
                'position_id'   => $request->get('position_id'),
                'from'          => date('Y-m-d')
            ]);
            
            EmployeeSalaryHistory::create([
                'employee_id'   => $employee->id, 
                'salary'        => $request->get('salary'),
                'from'          => date('Y-m-d')
            ]);

            DB::commit();
            $query = Employee::employeeInfoRelations()->findOrFail($employee->id);
            return response()->json(["item"=>$query]);
            
        } catch(Exception $exception) {
            DB::rollBack();
            return response()->json(["exception"=>$exception], 500);
        }
        
    }

    public function update(EmployeesStore $request, Employee $employee)
    {
        try {
            // $this->authorize('update', Employee::class);

            DB::beginTransaction();

            // $this->employeeService->updateContactInfo($request);
            // $this->employeeService->updateContactOther($request);
            // $this->employeeService->updateContactInfo($request);

            $contact = Contact::findOrFail($employee->contact_id);
            $contact->update($request->get('contact'));

            $employee->update(
                $request->only(['office_id', 'department_id'])
            );
            
            $lastSalary = EmployeeSalaryHistory::whereNull('to')->where('employee_id', $employee->id)->first();
            if ($lastSalary) {
                if ($lastSalary->salary != $request->get('salary')) {
                    $lastSalary->update(['to' => Carbon::now()]);
                    EmployeeSalaryHistory::create([
                        'employee_id'   => $employee->id, 
                        'salary'        => $request->get('salary'),
                        'from'          => Carbon::now()
                    ]);
                }
            } else if ($request->get('salary') > 0) {
                EmployeeSalaryHistory::create([
                    'employee_id'   => $employee->id, 
                    'salary'        => $request->get('salary'),
                    'from'          => Carbon::now()
                ]);
            }

            $lastPosition = EmployeeHistory::whereNull('to')->where('employee_id', $employee->id)->first();
            if ( $lastPosition ) {
                //
                if ($lastPosition->position_id != request('position_id')) {
                    request()->only([]);
                    $lastPosition->update(['to' => Carbon::now()]);
                    EmployeeHistory::create([
                        'employee_id'   => $employee->id, 
                        'position_id'   => $request->get('position_id'),
                        'from'          => Carbon::now()
                    ]);
                }
            } else if ($request->get('position_id') > 0) {
                EmployeeHistory::create([
                    'employee_id'   => $employee->id, 
                    'position_id'   => $request->get('position_id'),
                    'from'          => Carbon::now()
                ]);
            }

            DB::commit();
            $query = Employee::employeeInfoRelations()->findOrFail($employee->id);
            return ["item"=>$query];

        } catch(Exception $exception) {
            DB::rollBack();
            return response()->json(["exception"=>$exception], 500);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $employee = Employee::findOrFail($id);
            // $this->authorize('delete', $employee);            
            Contact::findOrFail($employee->contact_id)->delete();
            EmployeeSalaryHistory::where('employee_id', $employee->id)->delete();
            EmployeeHistory::where('employee_id', $employee->id)->delete();
            HolidayHistory::where('employee_id', $employee->id)->delete();                        
            $employee->delete();

            DB::commit();
            return response()->json([]);

        } catch(Exception $exception) {
            DB::rollBack();
            return response()->json(["exception"=>$exception], 500);
        }
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?Employee::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }

}
