<?php

namespace App\Http\Controllers;

use App\User;
use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\DocumentSalaryTransactionRequest;

class SalaryTransactionRequestsController extends Controller
{


    public function index()
    {

        $query = SalaryTransactionRequest::query();

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id){

        $item = SalaryTransactionRequest::findOrFail($id);

        // $this->authorize('view', $item);

        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', DocumentEmploymentContract::class);

        $validation = DocumentSalaryTransactionRequest::validation($request->salary_transaction_request);
        
        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;

            $document = Document::create([
                'title' => $request->title,
                'related_document_type' => 'salary_transaction_request',
                'employee_id' => $employee_id
            ]);

            $item = DocumentSalaryTransactionRequest::create(
                array_merge($request->salary_transaction_request, ['document_id'=>$document->id])
            );
            
            $document->salary_transaction_request = $item;
            return response()->json(["item"=>$document]);
        }

    }


    public function update(Request $request, $id)
    {
        // $this->authorize('create', DocumentEmploymentContract::class);

        $document = Document::findOrFail($request->id);
        $salary_transaction_request = DocumentSalaryTransactionRequest::findOrFail($id);

        $validation = DocumentSalaryTransactionRequest::validation($request->salary_transaction_request);
        
        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {

            $document->update([
                'title' => $request->title
            ]);

            $salary_transaction_request->update($request->salary_transaction_request);
            
            $document->salary_transaction_request = $salary_transaction_request;
            return response()->json(["item"=>$document]);
        }
    }

    public function createPDF($id, $url)
    {
        $document = Document::withDocumentContent()->findOrFail($id);
        if ($document->url == $url)
        {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pdf.salary-transaction-request', [
                'position' => $document->salaryTransactionRequest['position'],
                'first_name' => $document->salaryTransactionRequest['first_name'],
                'last_name' => $document->salaryTransactionRequest['last_name'],
                'company_name' => $document->salaryTransactionRequest['company_name'],
                'company_representative' => $document->salaryTransactionRequest['company_representative'],
                'date' => $document->salaryTransactionRequest['date'],
                'bank' => $document->salaryTransactionRequest['bank'],
                'bank_account_number' => $document->salaryTransactionRequest['bank_account_number'],
            ]);
            return $pdf->stream();
        } else {
            abort(404);
        }
    }

    public function destroy($id)
    {
        $salary_transaction_request = DocumentSalaryTransactionRequest::findOrFail($id);
        $document = Document::findOrFail($salary_transaction_request->document_id);
        // $this->authorize('delete', $item);

        $document->delete();
        $salary_transaction_request->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?SalaryTransactionRequest::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
