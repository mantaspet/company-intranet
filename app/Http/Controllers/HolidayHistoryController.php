<?php

namespace App\Http\Controllers;

use App\User;
use App\HolidayHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HolidayHistoryController extends Controller
{
    public function index()
    {
        $query = HolidayHistory::where('is_confirmed', true);

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getMyHolidayHistory() {
        $user_id = (Auth::check()) ? Auth::user()->id : 1;
        $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        $query = EmployeeSalaryHistory::where('employee_id', $employee_id)->responsibleEmployees();
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function myHistory() {
        // $this->authorize('myHistory', HolidayHistory::class);
        $user_id = (Auth::check()) ? Auth::user()->id : 1;
        $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        $items = HolidayHistory::where('employee_id', $employee_id)->responsibleEmployees();
        return response()->json(['items' => $items->paginate(50)]);
    }

    public function requestIndex()
    {
        $query = HolidayHistory::whereNull('is_confirmed')->responsibleEmployees();
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id){
        $item = HolidayHistory::where('employee_id', $id)->where('is_confirmed', true)->responsibleEmployees()->get();
        // $this->authorize('view', $item);

        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', HolidayHistory::class);
        $employee_id = $request->get('employee_id');
        if (!isset($employee_id)) {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $request['employee_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        }
        $validation = HolidayHistory::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {
            $holidayHistory = HolidayHistory::create($request->all());
            $query = HolidayHistory::responsibleEmployees()->findOrFail($holidayHistory->id);
            return response()->json(["item"=>$query]);
        }
    }


    public function update(Request $request, $id)
    {
        $holidayHistory = HolidayHistory::findOrFail($id);
        // $this->authorize('update', $holidayHistory);

        $validation = HolidayHistory::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {
            $is_confirmed = $request->get('is_confirmed');
            if (isset($is_confirmed)) {
                $request['confirmed_by'] = (Auth::check()) ? Auth::user()->id : 1;
                $holidayHistory->update($request->only(['from', 'to', 'employee_id', 'type', 'responsible_employee_id', 'confirmed_by', 'is_confirmed']));
            } else {
                $holidayHistory->update($request->only(['from', 'to', 'employee_id', 'type', 'responsible_employee_id']));                
            }
            $query = HolidayHistory::responsibleEmployees()->findOrFail($holidayHistory->id);
            return response()->json(["item"=>$query]);
        }
    }


    public function destroy($id)
    {
        $item = HolidayHistory::findOrFail($id);

        // $this->authorize('delete', $item);

        $item->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?HolidayHistory::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
