<?php

namespace App\Http\Controllers;

use App\User;
use App\EmployeeHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeHistoryController extends Controller
{

    public function index()
    {
        $query = EmployeeHistory::query();

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getMyEmploymentHistory() {
        $user_id = (Auth::check()) ? Auth::user()->id : 1;
        $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        $query = EmployeeHistory::where('employee_id', $employee_id)->employeePositions()->get();
        return response()->json(['item' => $query]);
    }

    public function show($id)
    {
        $item = EmployeeHistory::where('employee_id', $id)->employeePositions()->get();

        // $this->authorize('view', $item);

        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', EmployeeHistory::class);
        $validation = EmployeeHistory::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()], 400);

        } else {
            $employeeHistory = EmployeeHistory::create($request->only(['from', 'to', 'position_id', 'employee_id']));
            $query = EmployeeHistory::employeePositions()->findOrFail($employeeHistory->id);
            return response()->json(["item" => $query]);
        }
    }


    public function update(Request $request, $id)
    {
        $employeeHistory = EmployeeHistory::findOrFail($id);

        // $this->authorize('update', $item);

        $validation = EmployeeHistory::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()], 400);

        } else {
            $employeeHistory->update($request->only(['from', 'to', 'position_id', 'employee_id']));
            $query = EmployeeHistory::employeePositions()->findOrFail($employeeHistory->id);
            return response()->json(["item" => $query]);
        }
    }

    public function destroy($id)
    {
        $item = EmployeeHistory::findOrFail($id);

        // $this->authorize('delete', $item);

        $item->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?EmployeeHistory::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
