<?php

namespace App\Http\Controllers;

use App\User;
use App\Asset;
use App\AssetAssignment;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AssetAssignmentsController extends Controller
{
    // Administruoti prašymus
    public function getAssetRequests() 
    {
        $query = AssetAssignment::whereNull('is_confirmed')->assetAssignmentInfo();
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getMyRequests(Request $request) {
        $user_id = (Auth::check()) ? Auth::user()->id : 1;
        $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        $query = AssetAssignment::where('employee_id', $employee_id)->assetAssignmentInfo();
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getMyAssets(Request $request) {
        $user_id = (Auth::check()) ? Auth::user()->id : 1;
        $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;
        $query = AssetAssignment::where('employee_id', $employee_id)->where('is_confirmed', true)->whereNull('return_date')->assetAssignmentInfo();
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function returnAsset(Request $request) {
        $item = AssetAssignment::findOrFail($request->get('id'));
        // $this->authorize('update', $item);
        $item->update([
            'return_date' => Carbon::now(),
            'return_reason' => $request->get('return_reason')
        ]);
        Asset::findOrFail($item->asset_id)->update(['is_assigned' => 0]);
        return response()->json(["item"=>$item]);
    }

    public function newRequest(Request $request) {
        if ($request->get('type') == 'office') {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $request['office_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->office_id;
            $request['employee_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->id;
            $item = AssetAssignment::create($request->only(['request_message', 'deadline', 'office_id', 'employee_id']));
        } else if ($request->get('type') == 'employee') {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $request['employee_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->id;
            $item = AssetAssignment::create($request->only(['request_message', 'deadline', 'employee_id']));
        }
        return response()->json(["item"=>$item]);
    }

    public function updateRequest(Request $request) {
        if ($request->get('type') == 'office') {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $request['office_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->office_id;
            $request['employee_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->id;
            $item = AssetAssignment::findOrFail($request->get('id'));
            $item->update($request->only(['request_message', 'deadline', 'office_id', 'employee_id']));
        } else if ($request->get('type') == 'employee') {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $request['employee_id'] = User::where('id', $user_id)->withEmployee()->first()->employee->id;
            $request['office_id'] = null;
            $item = AssetAssignment::findOrFail($request->get('id'));
            $item->update($request->only(['request_message', 'deadline', 'employee_id', 'office_id']));
        }
        return response()->json(["item"=>$item]);
    }

    public function index()
    {
        $query = AssetAssignment::whereNull('return_date')->whereNotNull('asset_id')->whereNotNull('is_confirmed')->assetAssignmentInfo();
        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id) 
    {
        $item = AssetAssignment::assetAssignmentInfo()->findOrFail($id);
        // $this->authorize('view', $item);
        return response()->json(["item"=>$item]);
    }

    public function store(Request $request)
    {
        // $this->authorize('create', AssetAssignment::class);
        $request['confirmed_by'] = (Auth::check()) ? Auth::user()->id : 1;
        $id = AssetAssignment::create($request->all())->id;
        $item = AssetAssignment::assetAssignmentInfo()->findOrFail($id);
        Asset::findOrFail($item->asset_id)->update(['is_assigned' => 1]);
        return response()->json(["item"=>$item]);
    }

    public function update(Request $request, $id)
    {
        $item = AssetAssignment::findOrFail($id);
        // $this->authorize('update', $item);
        $validation = AssetAssignment::validation($request->all());
        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {
            $request['confirmed_by'] = (Auth::check()) ? Auth::user()->id : 1;
            $item->update($request->all());
            if (request('is_confirmed')) {
                Asset::findOrFail($item->asset_id)->update(['is_assigned' => 1]);                
            }
            return response()->json(["item"=>$item]);
        }
    }

    public function destroy($id)
    {
        $item = AssetAssignment::findOrFail($id);
        // $this->authorize('delete', $item);
        $item->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?AssetAssignment::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }

}
