<?php

namespace App\Http\Controllers;

use App\User;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{


    public function index()
    {
        $query = User::query();

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id)
    {
        $item = User::where('id', $id)->withEmployee()->first();

        // $this->authorize('view', $item);

        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', User::class);

        $validation = User::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {
            $user = User::where('email', request('email'))->first();
            if (isset($user)) {
                $user->update([
                    'role' => request('role'),
                    'is_active' => true
                ]);
                $employee = Employee::findOrFail(request('employee_id'));
                $employee->update(['user_id'=>$user->id]);
                return response()->json(["item"=>$employee]);
            } else {
                $password = request('password');
                $user = User::create([
                    'email' => request('email'),
                    'contact_id' => request('contact_id'),
                    'role' => request('role'),
                    'password' => bcrypt($password)
                ]);
                $employee = Employee::findOrFail(request('employee_id'));
                $employee->update(['user_id'=>$user->id]);
                return response()->json(["item"=>$employee]);
            }
        }
    }

    public function suspendUser(Request $request) 
    {
        $user = User::where('email', request('email'))->first();
        if (isset($user)) {
            $user->update(['is_active' => false]);
            $employee = Employee::findOrFail(request('employee_id'));
            $employee->update(['user_id'=>null]);
            return response()->json(['item'=>'Paskyra deaktyvuota.']);
        } else {
            return response()->json(['item'=>'Vartotojas nerastas.'], 500);
        }
    }

    public function resetPassword(Request $request)
    {
        $user = User::where('email', request('email'))->first();
        if (isset($user)) {
            $password = request('password');
            $user->update(['password' => bcrypt($password)]);
            return response()->json(['item'=>'Slaptažodis pakeistas.']);
        } else {
            return response()->json(['item'=>'Vartotojas nerastas.'], 500);
        }
    }
 
    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $password = request('current_password');
        if (Hash::check($password, $user->password)) {
            $new_password = request('new_password');
            $user->update([
                'password' => bcrypt($new_password)
            ]);
            return response()->json(['item'=>'Slaptažodis pakeistas']);
        } else {
            return response()->json(['item'=>'Neteisingas slaptažodis.'], 500);
        }
    }

    public function getAuthenticatedUser()
    {
        $user = Auth::User();
        return response()->json(['item'=>$user->withContact()->where('id', $user->id)->first()]);
    }


    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);

        // $this->authorize('update', $item);

        $validation = User::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $item->update($request->all());
            return response()->json(["item"=>$item]);
        }
    }

    public function destroy($id)
    {
        $item = User::findOrFail($id);

        // $this->authorize('delete', $item);

        $item->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?User::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }

    public function currentUser()
    {
        return request()->user()->load('contact');
    }


}
