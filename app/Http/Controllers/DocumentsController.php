<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use App\DocumentEmploymentContract;
use Illuminate\Support\Facades\App;
use App\DocumentNtiApplicationRequest;
use App\DocumentSalaryTransactionRequest;

class DocumentsController extends Controller
{


    public function index()
    {
        $query = Document::withDocumentContent();

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id)
    {
        $item = Document::findOrFail($id);
        // $this->authorize('view', $item);
        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', Document::class);

        $validation = Document::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $item = Document::create($request->all());
            return response()->json(["item"=>$item]);
        }
    }


    public function update(Request $request, $id)
    {
        $item = Document::findOrFail($id);

        // $this->authorize('update', $item);

        $validation = Document::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $item->update($request->all());
            return response()->json(["item"=>$item]);
        }
    }


    public function destroy($id)
    {
        $item = Document::findOrFail($id);

        // $this->authorize('delete', $item);

        $item->delete();
        return response()->json([]);
    }

    public function createPDF($id, $url)
    {
        $document = Document::withDocumentContent()->findOrFail($id);
        // return response()->json(["item"=>$document->salaryTransactionRequest['position']]);
        if ($document->url == $url)
        {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pdf.salary-transaction-request', [
                'position' => $document->salaryTransactionRequest['position'],
                'first_name' => $document->salaryTransactionRequest['first_name'],
                'last_name' => $document->salaryTransactionRequest['last_name'],
                'company_name' => $document->salaryTransactionRequest['company_name'],
                'company_representative' => $document->salaryTransactionRequest['company_representative'],
                'date' => $document->salaryTransactionRequest['date'],
                'bank' => $document->salaryTransactionRequest['bank'],
                'bank_account_number' => $document->salaryTransactionRequest['bank_account_number'],
            ]);
            // $pdf->loadView('pdf.test', ['var_name'=>$document->salary_transaction_request['position']]);
            return $pdf->stream();
        } else {
            abort(404);
        }
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?Document::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
