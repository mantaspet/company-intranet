<?php

namespace App\Http\Controllers;

use App\User;
use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\DocumentNtiApplicationRequest;

class NtiApplicationRequestsController extends Controller
{


    public function index()
    {

        $query = NtiApplicationRequest::query();

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id){

        $item = NtiApplicationRequest::findOrFail($id);

        // $this->authorize('view', $item);

        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', DocumentEmploymentContract::class);

        $validation = DocumentNtiApplicationRequest::validation($request->nti_application_request);
        
        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;

            $document = Document::create([
                'title' => $request->title,
                'related_document_type' => 'nti_application_request',
                'employee_id' => $employee_id
            ]);

            $item = DocumentNtiApplicationRequest::create(
                array_merge($request->nti_application_request, ['document_id'=>$document->id])
            );
            
            $document->nti_application_request = $item;
            return response()->json(["item"=>$document]);
        }

    }


    public function update(Request $request, $id)
    {
        // $this->authorize('create', DocumentEmploymentContract::class);

        $document = Document::findOrFail($request->id);
        $nti_application_request = DocumentNtiApplicationRequest::findOrFail($id);

        $validation = DocumentNtiApplicationRequest::validation($request->nti_application_request);
        
        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {

            $document->update([
                'title' => $request->title
            ]);

            $nti_application_request->update($request->nti_application_request);
            
            $document->nti_application_request = $nti_application_request;
            return response()->json(["item"=>$document]);
        }
    }

    public function createPDF($id, $url)
    {
        $document = Document::withDocumentContent()->findOrFail($id);
        if ($document->url == $url)
        {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pdf.nti-application-request', [
                'company_name' => $document->ntiApplicationRequest['company_name'],
                'first_name' => $document->ntiApplicationRequest['first_name'],
                'last_name' => $document->ntiApplicationRequest['last_name'],
                'nin' => $document->ntiApplicationRequest['nin'],
                'country' => $document->ntiApplicationRequest['country'],
                'city' => $document->ntiApplicationRequest['city'],
                'address' => $document->ntiApplicationRequest['address'],
                'request_date' => $document->ntiApplicationRequest['request_date'],
                'nti_application_date' => $document->ntiApplicationRequest['nti_application_date'],
                'children_info' => $document->ntiApplicationRequest['children_info'],
            ]);
            return $pdf->stream();
        } else {
            abort(404);
        }
    }

    public function destroy($id)
    {
        $nti_application_request = DocumentNtiApplicationRequest::findOrFail($id);
        $document = Document::findOrFail($nti_application_request->document_id);
        // $this->authorize('delete', $item);

        $document->delete();
        $nti_application_request->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?NtiApplicationRequest::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
