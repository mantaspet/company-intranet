<?php

namespace App\Http\Controllers;

use App\Asset;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AssetsController extends Controller
{


    public function index()
    {
        $query = Asset::query();

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getUnassignedAssets()
    {
        $query = Asset::where('is_assigned', 0);

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query->paginate(50)]);
    }

    public function getExpenses(Request $request)
    {
        $start = $request->get('start');
        $end = $request->get('end');
        if (isset($start) && isset($end)) {
            $query = Asset::whereBetween('acquisition_date', [$start, $end])->get();
        } else if (isset($start) && !isset($end)) {
            $query = Asset::where('acquisition_date', '>=', $start)->get();
        } else if (!isset($start) && isset($end)) {
            $query = Asset::where('acquisition_date', '<=', $end)->get();
        } else {
            $query = Asset::all();
        }

        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');

        return response()->json(['items' => $query]);
    }

    public function show($id){

        $item = Asset::findOrFail($id);

        // $this->authorize('view', $item);

        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', Asset::class);

        $validation = Asset::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $acquisition_date = $request->get('acquisition_date');
            $value = $request->get('value');
            if (isset($acquisition_date)) {
                $item = Asset::create($request->all());
            } else if (isset($value) && is_numeric($value)) {
                $item = Asset::create([
                    'name'              => $request->get('name'),
                    'value'             => $request->get('value'),
                    'type'              => $request->get('type'),
                    'acquisition_date'  => Carbon::now()
                ]);
            } else {
                $item = Asset::create([
                    'name'              => $request->get('name'),
                    'type'              => $request->get('type'),
                    'acquisition_date'  => Carbon::now()
                ]);
            }
            return response()->json(["item"=>$item]);
        }
    }


    public function update(Request $request, $id)
    {
        $item = Asset::findOrFail($id);
        $oldValue = $item->value;

        // $this->authorize('update', $item);

        $validation = Asset::validation($request->all());

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $item->update($request->all());
            $item->oldValue = $oldValue;
            return response()->json(["item"=>$item]);
        }
    }


    public function destroy($id)
    {
        $item = Asset::findOrFail($id);

        // $this->authorize('update', $item);

        $item->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?Asset::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
