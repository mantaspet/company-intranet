<?php

namespace App\Http\Controllers;

use App\User;
use App\Document;
use Illuminate\Http\Request;
use App\DocumentEmploymentContract;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class EmploymentContractsController extends Controller
{


    public function index()
    {
        $query = DocumentEmploymentContract::query();
        //$query->orderByRaw('FIELD(status, "not_approved", "payment_pending", "payment_received", "discarded")');
        return response()->json(['items' => $query->paginate(50)]);
    }

    public function show($id)
    {
        $item = DocumentEmploymentContract::findOrFail($id);
        // $this->authorize('view', $item);
        return response()->json(["item"=>$item]);
    }


    public function store(Request $request)
    {
        // $this->authorize('create', DocumentEmploymentContract::class);

        $validation = DocumentEmploymentContract::validation($request->employment_contract);

        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);

        } else {
            $user_id = (Auth::check()) ? Auth::user()->id : 1;
            $employee_id = User::where('id', $user_id)->withEmployee()->first()->employee->id;

            $document = Document::create([
                'title' => $request->title,
                'related_document_type' => 'employment_contract',
                'employee_id' => $employee_id
            ]);
            // $newarray = array_merge($request->employment_contract, ['document_id'=>$document->id]);
            $item = DocumentEmploymentContract::create(
                array_merge($request->employment_contract, ['document_id'=>$document->id])
            );
            
            $document->employment_contract = $item;
            return response()->json(["item"=>$document]);
        }
    }

    public function update(Request $request, $id)
    {
        // $this->authorize('create', DocumentEmploymentContract::class);

        $document = Document::findOrFail($request->id);
        $employment_contract = DocumentEmploymentContract::findOrFail($id);

        $validation = DocumentEmploymentContract::validation($request->employment_contract);
        
        if ($validation->fails()) {
            return response()->json(["status" => "errors", "messages" => $validation->messages()],400);
        } else {

            $document->update([
                'title' => $request->title
            ]);

            $employment_contract->update($request->employment_contract);
            
            $document->employment_contract = $employment_contract;
            return response()->json(["item"=>$document]);
        }
    }

    public function createPDF($id, $url)
    {
        $document = Document::withDocumentContent()->findOrFail($id);
        if ($document->url == $url)
        {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('pdf.employment-contract', [
                'date' => $document->employmentContract['date'],
                'employer' => $document->employmentContract['employer'],
                'employer_code' => $document->employmentContract['employer_code'],
                'employer_representative' => $document->employmentContract['employer_representative'],
                'employer_representative_nin' => $document->employmentContract['employer_representative_nin'],
                'employee_first_name' => $document->employmentContract['employee_first_name'],
                'employee_last_name' => $document->employmentContract['employee_last_name'],
                'employee_identity_card_number' => $document->employmentContract['employee_identity_card_number'],
                'employee_identity_card_issuance_date' => $document->employmentContract['employee_identity_card_issuance_date'],
                'employee_nin' => $document->employmentContract['employee_nin'],
                'employee_address' => $document->employmentContract['employee_address'],
                'employee_position' => $document->employmentContract['employee_position'],
                'employee_salary' => $document->employmentContract['employee_salary'],
                'employee_salary_comment' => $document->employmentContract['employee_salary_comment'],
                'valid_from' => $document->employmentContract['valid_from'],
                'started_from' => $document->employmentContract['started_from']
            ]);
            return $pdf->stream();
        } else {
            abort(404);
        }
    }

    public function destroy($id)
    {
        $employment_contract = DocumentEmploymentContract::findOrFail($id);
        $document = Document::findOrFail($employment_contract->document_id);
        // $this->authorize('delete', $item);

        $document->delete();
        $employment_contract->delete();
        return response()->json([]);
    }

    public function find($term)
    {
        return response()->json(['items'=>(strlen($term)>2)?DocumentEmploymentContract::where('title', 'LIKE', '%$term%')->limit(20)->get():[]]);
    }


}
