<?php

namespace App\Http\Middleware;

use Closure;

class OnlyActiveUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->contact->status != 'dirba') {
            return response()->json('unauthorized', 403);
        }
        return $next($request);
    }
}
