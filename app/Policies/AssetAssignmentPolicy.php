<?php

namespace App\Policies;

use App\User;
use App\AssetAssignment;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetAssignmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the assetAssignment.
     *
     * @param  \App\User  $user
     * @param  \App\AssetAssignment  $assetAssignment
     * @return mixed
     */
    public function view(User $user, AssetAssignment $assetAssignment)
    {
        return true;
    }

    /**
     * Determine whether the user can create assetAssignments.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the assetAssignment.
     *
     * @param  \App\User  $user
     * @param  \App\AssetAssignment  $assetAssignment
     * @return mixed
     */
    public function update(User $user, AssetAssignment $assetAssignment)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the assetAssignment.
     *
     * @param  \App\User  $user
     * @param  \App\AssetAssignment  $assetAssignment
     * @return mixed
     */
    public function delete(User $user, AssetAssignment $assetAssignment)
    {
        return false;
    }
}
