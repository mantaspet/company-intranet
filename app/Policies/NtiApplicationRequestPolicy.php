<?php

namespace App\Policies;

use App\User;
use App\NtiApplicationRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class NtiApplicationRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the ntiApplicationRequest.
     *
     * @param  \App\User  $user
     * @param  \App\NtiApplicationRequest  $ntiApplicationRequest
     * @return mixed
     */
    public function view(User $user, NtiApplicationRequest $ntiApplicationRequest)
    {
        //
    }

    /**
     * Determine whether the user can create ntiApplicationRequests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the ntiApplicationRequest.
     *
     * @param  \App\User  $user
     * @param  \App\NtiApplicationRequest  $ntiApplicationRequest
     * @return mixed
     */
    public function update(User $user, NtiApplicationRequest $ntiApplicationRequest)
    {
        //
    }

    /**
     * Determine whether the user can delete the ntiApplicationRequest.
     *
     * @param  \App\User  $user
     * @param  \App\NtiApplicationRequest  $ntiApplicationRequest
     * @return mixed
     */
    public function delete(User $user, NtiApplicationRequest $ntiApplicationRequest)
    {
        //
    }
}
