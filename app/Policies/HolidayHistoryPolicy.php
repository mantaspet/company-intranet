<?php

namespace App\Policies;

use App\User;
use App\HolidayHistory;
use Illuminate\Auth\Access\HandlesAuthorization;

class HolidayHistoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the holidayHistory.
     *
     * @param  \App\User  $user
     * @param  \App\HolidayHistory  $holidayHistory
     * @return mixed
     */
    public function view(User $user, HolidayHistory $holidayHistory)
    {
        return true;
    }

    public function myHistory(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can create holidayHistorys.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the holidayHistory.
     *
     * @param  \App\User  $user
     * @param  \App\HolidayHistory  $holidayHistory
     * @return mixed
     */
    public function update(User $user, HolidayHistory $holidayHistory)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the holidayHistory.
     *
     * @param  \App\User  $user
     * @param  \App\HolidayHistory  $holidayHistory
     * @return mixed
     */
    public function delete(User $user, HolidayHistory $holidayHistory)
    {
        return true;
    }
}
