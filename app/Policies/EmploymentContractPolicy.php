<?php

namespace App\Policies;

use App\User;
use App\EmploymentContract;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmploymentContractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the employmentContract.
     *
     * @param  \App\User  $user
     * @param  \App\EmploymentContract  $employmentContract
     * @return mixed
     */
    public function view(User $user, EmploymentContract $employmentContract)
    {
        //
    }

    /**
     * Determine whether the user can create employmentContracts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the employmentContract.
     *
     * @param  \App\User  $user
     * @param  \App\EmploymentContract  $employmentContract
     * @return mixed
     */
    public function update(User $user, EmploymentContract $employmentContract)
    {
        //
    }

    /**
     * Determine whether the user can delete the employmentContract.
     *
     * @param  \App\User  $user
     * @param  \App\EmploymentContract  $employmentContract
     * @return mixed
     */
    public function delete(User $user, EmploymentContract $employmentContract)
    {
        //
    }
}
