<?php

namespace App\Policies;

use App\User;
use App\EmployeeSalaryHistory;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeSalaryHistoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the EmployeeSalaryHistory.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeSalaryHistory  $EmployeeSalaryHistory
     * @return mixed
     */
    public function view(User $user, EmployeeSalaryHistory $EmployeeSalaryHistory)
    {
        return true;
    }

    /**
     * Determine whether the user can create EmployeeSalaryHistory.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the EmployeeSalaryHistory.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeSalaryHistory  $EmployeeSalaryHistory
     * @return mixed
     */
    public function update(User $user, EmployeeSalaryHistory $EmployeeSalaryHistory)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the EmployeeSalaryHistory.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeSalaryHistory  $EmployeeSalaryHistory
     * @return mixed
     */
    public function delete(User $user, EmployeeSalaryHistory $EmployeeSalaryHistory)
    {
        return true;
    }
}
