<?php

namespace App\Policies;

use App\User;
use App\EmployeeHistory;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeHistoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the EmployeeHistory.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeHistory  $EmployeeHistory
     * @return mixed
     */
    public function view(User $user, EmployeeHistory $EmployeeHistory)
    {
        //
    }

    /**
     * Determine whether the user can create EmployeeHistory.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the EmployeeHistory.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeHistory  $EmployeeHistory
     * @return mixed
     */
    public function update(User $user, EmployeeHistory $EmployeeHistory)
    {
        //
    }

    /**
     * Determine whether the user can delete the EmployeeHistory.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeHistory  $EmployeeHistory
     * @return mixed
     */
    public function delete(User $user, EmployeeHistory $EmployeeHistory)
    {
        //
    }
}
