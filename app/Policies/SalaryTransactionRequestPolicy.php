<?php

namespace App\Policies;

use App\User;
use App\SalaryTransactionRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class SalaryTransactionRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the salaryTransactionRequest.
     *
     * @param  \App\User  $user
     * @param  \App\SalaryTransactionRequest  $salaryTransactionRequest
     * @return mixed
     */
    public function view(User $user, SalaryTransactionRequest $salaryTransactionRequest)
    {
        //
    }

    /**
     * Determine whether the user can create salaryTransactionRequests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the salaryTransactionRequest.
     *
     * @param  \App\User  $user
     * @param  \App\SalaryTransactionRequest  $salaryTransactionRequest
     * @return mixed
     */
    public function update(User $user, SalaryTransactionRequest $salaryTransactionRequest)
    {
        //
    }

    /**
     * Determine whether the user can delete the salaryTransactionRequest.
     *
     * @param  \App\User  $user
     * @param  \App\SalaryTransactionRequest  $salaryTransactionRequest
     * @return mixed
     */
    public function delete(User $user, SalaryTransactionRequest $salaryTransactionRequest)
    {
        //
    }
}
