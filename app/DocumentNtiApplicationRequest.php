<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentNtiApplicationRequest extends Model {
    use SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "document_id",
        "company_name",
        "first_name",
        "last_name",
        "nin",
        "country",
        "city",
        "address",
        "request_date",
        "nti_application_date",
        "children_info"
    ];

    // Relations
    // --------------------------------------------------------------

    public function document() {
        return $this->belongsTo('App\Document', 'id', 'document_id');
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "company_name" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "nin" => "required",
            "country" => "required",
            "city" => "required",
            "address" => "required",
            "request_date" => "required|date",
            "nti_application_date" => "required|date"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
