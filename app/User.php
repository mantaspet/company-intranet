<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use SoftDeletes;

    use HasApiTokens, Notifiable;

    const AVAILABLE_ROLES = [
        'administrator',
        'manager',
        'employee'
    ];

    protected $hidden = [
        'password',
        "remember_token",
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $fillable = [
        "email",
        "is_active",
        "password",
        "role",
        "contact_id"
    ];

    // Relations
    // ----------------------------------------------------------------------------------------------

    public function contact() {
        return $this->hasOne('App\Contact', 'id', 'contact_id');
    }

    public function employee() {
        return $this->hasOne('App\Employee');
    }

    // ----------------------------------------------------------------------------------------------

    public function scopeWithEmployee($query) {
        return $query->with(['employee']);
    }

    public function scopeWithContact($query) {
        return $query->with(['contact']);
    }

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "email" => "required",
            "role" => "required|in:".implode(',', self::AVAILABLE_ROLES),
            "contact_id" => "required|integer"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
