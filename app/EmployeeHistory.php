<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeHistory extends Model {
    use SoftDeletes;
    protected $table = 'employee_history';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "from",
        "to",
        "position_id",
        "employee_id"
    ];

    // Relations
    // --------------------------------------------------------------

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function position() {
        return $this->hasOne('App\Position', 'id', 'position_id');
    }

    // --------------------------------------------------------------

    public function scopeEmployeePositions($query) {
        return $query->with(['position']);
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "from" => "required|date",
            "position_id" => "required|integer",
            "employee_id" => "required|integer"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
