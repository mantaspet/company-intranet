<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model {
    use SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "name"
    ];

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "name" => "required"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
