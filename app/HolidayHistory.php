<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class HolidayHistory extends Model {
    use SoftDeletes;
    protected $table = 'holiday_history';

    const HOLIDAY_TYPES = [
        'Atostogos',
        'Biuletenis',
        'Motinystės atostogos'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "is_confirmed",
        "from",
        "to",
        "employee_id",
        "type",
        "responsible_employee_id",
        "confirmed_by"
    ];

    protected $casts = [
        "is_confirmed" => "boolean"
    ];

    // Relations
    // --------------------------------------------------------------

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function responsible_employee() {
        return $this->hasOne('App\Employee', 'id', 'responsible_employee_id');
    }

    public function confirmedBy() {
        return $this->hasOne('App\User', 'id', 'confirmed_by');
    }

    // --------------------------------------------------------------

    public function scopeResponsibleEmployees($query) {
        return $query->with(['responsible_employee'=>function($query){
            $query->with('contact');
        }, 'confirmedBy'=>function($query){
            $query->with('contact');
        }, 'employee'=>function($query){
            $query->with('contact');
        }]);
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "from" => "required|date",
            "employee_id" => "required|integer",
            "type" => "required|in:".implode(',', self::HOLIDAY_TYPES),
            "responsible_employee_id" => "required|integer"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
