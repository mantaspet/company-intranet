<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Carbon\Carbon;
use Route;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\User' => 'App\Policies\UserPolicy',
        'App\SalaryTransactionRequest' => 'App\Policies\SalaryTransactionRequestPolicy',
        'App\Position' => 'App\Policies\PositionPolicy',
        'App\Office' => 'App\Policies\OfficePolicy',
        'App\NtiApplicationRequest' => 'App\Policies\NtiApplicationRequestPolicy',
        'App\HolidayHistory' => 'App\Policies\HolidayHistoryPolicy',
        'App\EmploymentContract' => 'App\Policies\EmploymentContractPolicy',
        'App\Employee' => 'App\Policies\EmployeePolicy',
        'App\EmployeeSalaryHistory' => 'App\Policies\EmployeeSalaryHistoryPolicy',
        'App\EmployeeHistory' => 'App\Policies\EmployeeHistoryPolicy',
        'App\Document' => 'App\Policies\DocumentPolicy',
        'App\Department' => 'App\Policies\DepartmentPolicy',
        'App\Contact' => 'App\Policies\ContactPolicy',
        'App\Asset' => 'App\Policies\AssetPolicy',
        'App\AssetAssignment' => 'App\Policies\AssetAssignmentPolicy',
        'App\Model' => 'App\Policies\ModelPolicy',
];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Route::group([ 'middleware' => 'cors' ], function() {
            Passport::routes();
        });

        Passport::tokensExpireIn(Carbon::now()->addDays(7));
        
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(7));

        //
    }
}
