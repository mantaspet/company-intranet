<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model {
    use SoftDeletes;

    const AVAILABLE_GENDERS = [
        'm',
        'f',
        'o'
    ];

    const AVAILABLE_STATUSES = [
        'dirba',
        'atostogauja',
        'nebedirba'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "nin",
        "first_name",
        "last_name",
        "email",
        "phone",
        "status",
        "dob",
        "gender",
        "country",
        "city",
        "address"
    ];

    // Relations
    // ----------------------------------------------------------------------------------------------

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function office() {
        return $this->belongsTo('App\Office', 'office_id', 'id');
    }

    // ----------------------------------------------------------------------------------------------


    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function employeeValidation($data) {
        $rules = [
            "nin" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "dob" => "required|date",
            "email" => "required|email",
            "phone" => "required",
            "status" => "required|in:".implode(',', self::AVAILABLE_STATUSES),
            "gender" => "required|in:".implode(',', self::AVAILABLE_GENDERS),
            "country" => "required",
            "city" => "required",
            "address" => "required"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

    static function userValidation($data) {
        $rules = [
            "first_name" => "required",
            "email" => "required|email"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
