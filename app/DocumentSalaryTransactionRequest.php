<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentSalaryTransactionRequest extends Model {
    use SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "document_id",
        "position",
        "first_name",
        "last_name",
        "company_name",
        "company_representative",
        "date",
        "bank",
        "bank_account_number"
    ];

    // Relations
    // --------------------------------------------------------------

    public function document() {
        return $this->belongsTo('App\Document', 'id', 'document_id');
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {

        $rules = [
            "position" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "company_name" => "required",
            "company_representative" => "required",
            "date" => "required|date",
            "bank" => "required",
            "bank_account_number" => "required"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
