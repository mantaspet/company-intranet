<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model {
    use SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $fillable = [
        "user_id",
        "contact_id",
        "office_id",
        "department_id"
    ];

    // Relations
    // ----------------------------------------------------------------------------------------------

    public function office() {
        return $this->belongsTo('App\Office', 'office_id', 'id');
    }

    public function department() {
        return $this->belongsTo('App\Department', 'department_id', 'id');
    }

    public function contact() {
        return $this->hasOne('App\Contact', 'id', 'contact_id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function employeeHistory() {
        return $this->hasMany('App\EmployeeHistory', 'employee_id', 'id');
    }

    public function employeeSalaryHistory() {
        return $this->hasMany('App\EmployeeSalaryHistory', 'employee_id', 'id')->orderBy('from');
    }

    public function holidayHistory() {
        return $this->hasMany('App\HolidayHistory', 'employee_id', 'id');
    }

    public function assignedAssets() {
        return $this->hasMany('App\AssetAssignment', 'employee_id', 'id');
    }

    // ----------------------------------------------------------------------------------------------

    public function scopeHavingAssets($query) {
        return $query->doesntHave('assignedAssets');
    }

    public function scopeEmployeeInfoRelations($query) {
        return $query->with(['contact', 'office', 'user', 'department', 'employeeHistory'=>function($query){
            $query->with('position')->whereNull('to');
        }, 'employeeSalaryHistory'=>function($query){
            $query->whereNull('to');
        }]);
    }

//     public function scopeEmployeeInfoRelations($query) {
//         return $query->with(['contact', 'office', 'user', 'department', 'assignedAssets'=>function($query){
// //            $query->with('asset')->whereNull('return_date');
//             $query->with('asset')->whereRaw(DB::raw('return_date IS NOT NULL'));
//         }, 'employeeHistory'=>function($query){
//             $query->with('position')->whereNull('to');
//         }]);
//     }



    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {

        // $validation = Validator::make($input = $data, $rules);

        // return $validation;
    }

}
