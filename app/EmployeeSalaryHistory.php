<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeSalaryHistory extends Model {
    use SoftDeletes;
    protected $table = 'employee_salary_history';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "salary",
        "from",
        "to",
        "is_fixed",
        "variable_part_description",
        "employee_id"
    ];
    
    protected $casts = [
        "is_fixed" => "boolean"
    ];

    // Relations
    // --------------------------------------------------------------

    public function employee() {
        return $this->belongsTo('App\Employee', 'id', 'employee_id');
    }

    // --------------------------------------------------------------

    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {
        $rules = [
            "salary" => "required|numeric",
            "from" => "required|date",
            "is_fixed" => "required|boolean",
            "employee_id" => "required|integer"
        ];

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
