<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetAssignment extends Model {
    use SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

	protected $fillable = [
        "request_message",
        "is_confirmed",
        "deadline",
        "assignment_date",
        "return_date",
        "return_reason",
        "employee_id",
        "office_id",
        "asset_id",
        "confirmed_by"
    ];

    protected $casts = [
        "is_confirmed" => "boolean"
    ];

    // Relations
    // --------------------------------------------------------------

    public function asset() {
        return $this->hasOne('App\Asset', 'id', 'asset_id');
    }

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function office() {
        return $this->belongsTo('App\Office', 'office_id', 'id');
    }

    public function confirmedByUser() {
        return $this->hasOne('App\User', 'id', 'confirmed_by');        
    }

    // --------------------------------------------------------------

    public function scopeAssetAssignmentInfo($query) {
        return $query->with(['asset', 'office', 'employee'=>function($query) {
            $query->with(['contact'=>function($query) {
                $query->select('id', 'first_name', 'last_name');
            }]);
        }, 'confirmedByUser'=>function($query) {
            $query->with('contact');
        }]);
    }
    
    // --------------------------------------------------------------


    protected static function boot() {
        parent::boot();

        static::creating(function($table) {
            $table->created_by = (Auth::check())?Auth::user()->id:1;
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::updating(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::saving(function($table) {
            $table->updated_by = (Auth::check())?Auth::user()->id:1;
        });

        static::deleting(function($table) {
            $table->deleted_by = (Auth::check())?Auth::user()->id:1;
            $table->save();
        });
    }

    static function validation($data) {

        $rules = [
            "is_confirmed" => "boolean"
        ];

        // Validator::extend('employee_or_office', function ($attribute, $value, $parameters, $validator) {
        //     return !(($parameters['employee_id'] == null && $parameters['office_id'] == null) ||
        //              ($parameters['employee_id'] != null && $parameters['office_id'] != null));
        // });

        $validation = Validator::make($input = $data, $rules);

        return $validation;
    }

}
