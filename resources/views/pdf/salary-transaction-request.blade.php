<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 12px;
        }
        tr {
            height: 13px;
        }
    </style>
</head>
<body>
    <table border="0">
        <tbody>
            <tr>
                <td colspan="3" align="center"><b>{{ $position }}</b></td>
            </tr>
            <tr>
                <td colspan="3" align="center"><b>{{ $first_name.' '.$last_name }}</b></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="left">Įmonė: <b>{{ $company_name }}</b></td>
            </tr>
            <tr>
                <td colspan="3" align="left">Direktorius: <b>{{ $company_representative }}</b></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center"><b><h3>PRAŠYMAS</h3></b></td>
            </tr>
            <tr>
                <td colspan="3" align="center"><b><h3>DĖL DARBO UŽMOKESČIO MOKĖJIMO</h3></b></td>
            </tr>
            <tr>
                <td colspan="3" align="center">{{ $date }}</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">Pra&scaron;au nuo {{ $date }} mokėti man darbo užmokestį 1 (vieną) kartą per mėnesį. Darbo užmokestį pra&scaron;au pervesti į sąskaitą banke:</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">Bankas: &bdquo;{{ $bank }}&ldquo;</td>
            </tr>
            <tr>
                <td colspan="3">Atsiskaitomosios sąskaitos numeris:  &bdquo;{{ $bank_account_number }}&ldquo;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td align="left" colspan="1">{{ $position }}</td>
                <td align="center" colspan="1"></td>
                <td align="right" colspan="1">{{ $first_name.' '.$last_name }}</td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
    
</body>
</html>