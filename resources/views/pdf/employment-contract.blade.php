<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <div align="right">
        <p>PATVIRTINTA</p>
        <p>Lietuvos Respublikos</p>
        <p>socialinės apsaugos ir darbo ministro</p>
        <p>2017 m. birželio 29 d. įsakymu Nr. A1-343</p>
    </div>
    
    <div align="center">
        <h3><strong>DARBO SUTARTIS</strong></h3>
        <p>{{ $date }}</p>
    </div>

    <p>Darbdavys:<br>{{ $employer }}, {{ $employer_code }}</p>
    <p>atstovaujamas:<br>{{ $employer_representative }}, {{ $employer_representative_nin }}</p>
    <p>ir darbuotojas:<br>{{ $employee_first_name }} {{ $employee_last_name }}, {{ $employee_nin }}, {{ $employee_address }}<br>Tapatybės kortelės nr.: {{ $employee_identity_card_number }}, 
    išdavimo data: {{ $employee_identity_card_issuance_date }}
    </p>

    <p>sudarė &scaron;ią darbo sutartį:</p>
    <p>&nbsp;</p>

    <p>Darbuotojas priimamas dirbti &scaron;iomis būtinosiomis darbo sutarties sąlygomis:</p>
    <ol>
        <li>Darbo vieta: {{ $employer }}.</li>
        <li>Pareigos: {{ $employee_position }}.</li>
        <li>Atlyginimas: {{ $employee_salary }} &euro;. {{ $employee_salary_comment ? '('.$employee_salary_comment.')' : '' }}</li>
        <li>Darbo sutartis įsigalioja ir darbuotojas pradeda dirbti: {{ $valid_from }}.</li>
        <li>Kasmetinių atostogų suteikimo trukmė, suteikimo tvarka ir apmokėjimo sąlygos nustatomos pagal Lietuvos Respublikos darbo kodekso 126&ndash;130 straipsnių nuostatas.</li>
        <li>Darbuotojas draudžiamas valstybiniu socialiniu draudimu. Valstybinio socialinio draudimo i&scaron;mokas ir paslaugas nustato atitinkamas valstybinio socialinio draudimo rū&scaron;is reglamentuojantys teisės aktai.</li>
        <li>Įspėjimo terminas, kai darbo sutartis nutraukiama darbdavio ar darbuotojo iniciatyva arba kitais atvejais, nustatomas pagal Lietuvos Respublikos darbo kodekso 55&ndash;57, 59, 61 ir 62 straipsnių nuostatas.</li>
        <li>&Scaron;i darbo sutartis gali būti pakeista ar papildyta ra&scaron;ti&scaron;ku &scaron;alių susitarimu, i&scaron;skyrus Lietuvos Respublikos darbo kodekse numatytus atvejus.</li>
        <li>&Scaron;i darbo sutartis gali būti nutraukta Lietuvos Respublikos darbo kodekso nustatytais pagrindais.</li>
        <li>Ginčai dėl &scaron;ios darbo sutarties nagrinėjami Lietuvos Respublikos darbo kodekso nustatyta tvarka.</li>
        <li>&Scaron;i darbo sutartis sudaroma dviem egzemplioriais: vienas pateikiamas darbdaviui, kitas &ndash; darbuotojui.</li>
        <li>Sutarties &scaron;alių para&scaron;ai:</li>
    </ol>

    <p>&nbsp;</p>
    <p>Darbdavys &ndash; fizinis asmuo arba darbdavio atstovas (vardas, pavardė, parašas)<br>{{ $employer_representative }}</p>
    <p>&nbsp;</p>
    <p>Darbuotojas &ndash; (vardas, pavardė, parašas)<br>{{ $employee_first_name }} {{ $employee_last_name }}</p>
</body>
</html>