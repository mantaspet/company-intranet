<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 12px;
        }
        tr {
            height: 13px;
        }
    </style>
</head>
<body>

    <div align="center">
        <strong><h3>{{ $company_name }}</h3></strong>
        <strong><h3>BUHALTERIJAI</strong></h3>
        <p>&nbsp;</p>
        <p>{{ $first_name }} {{ $last_name }}</p>
        <p>{{ $nin }}</p>
        <p>{{ $address }}, {{ $city }}, {{ $country }}</p>
        <p>&nbsp;</p>
        <p><h3><strong>PRA&Scaron;YMAS</strong></h3></p>
        <p>{{ $request_date }}</p>
    </div>
    
    <p>&nbsp;</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pra&scaron;au įmonėje {{ $company_name }} gaunamas su darbo santykiais&nbsp; susijusias pajamas nuo {{ $nti_application_date }} apmokestinti taikant pagrindinį neapmokestinamąjį pajamų dydį (NPD).</p>
    <p>&nbsp;</p>
    <p>{{ $children_info ? 'Informacija apie vaikus (iki 18 m.):' : ''}}</p>
    <p>{{ $children_info ? $children_info : ''}}</p>
    <p>&nbsp;</p>
    <p align="right">{{ $first_name }} {{ $last_name }}</p>
</body>
</html>