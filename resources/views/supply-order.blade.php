<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Supply order</title>
</head>
<body>
    <h1>Materials ordered!</h1>
    <ul>
        @foreach($materials as $material)
            <li>{{ $material->material_type }} - {{ $material->material_quantity }}</li>
        @endforeach
    </ul>
</body>
</html>