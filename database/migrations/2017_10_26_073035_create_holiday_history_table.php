<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidayHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday_history', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_confirmed')->nullable();
            $table->date('from');
            $table->date('to');
            $table->integer('employee_id');
            $table->string('type')->default('Atostogos');
            $table->integer('responsible_employee_id');
            $table->integer('confirmed_by')->nullable();
            
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holiday_history');
    }
}
