<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryTransactionRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_salary_transaction_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('company_name');
            $table->string('company_representative');
            $table->date('date');
            $table->string('bank');
            $table->string('bank_account_number');
            
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_salary_transaction_requests');
    }
}
