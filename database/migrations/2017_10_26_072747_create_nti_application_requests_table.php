<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNtiApplicationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_nti_application_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nin');
            $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->date('request_date');
            $table->date('nti_application_date');
            $table->string('children_info')->nullable();

            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_nti_application_requests');
    }
}
