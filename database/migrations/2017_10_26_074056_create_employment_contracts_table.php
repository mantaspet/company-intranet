<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_employment_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('registration_number');
            $table->string('employer');
            $table->string('employer_code');
            $table->string('employer_representative');
            $table->string('employer_representative_nin');
            $table->string('employee_first_name');
            $table->string('employee_last_name');
            $table->string('employee_identity_card_number');
            $table->date('employee_identity_card_issuance_date');
            $table->string('employee_nin');
            $table->string('employee_address');
            $table->string('employee_position');
            $table->string('employee_salary');
            $table->string('employee_salary_comment')->nullable();
            $table->date('valid_from');
            $table->date('started_from');
            
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_employment_contracts');
    }
}
