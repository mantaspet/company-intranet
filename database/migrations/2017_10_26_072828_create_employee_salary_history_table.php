<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalaryHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salary_history', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('salary', 10, 2);
            $table->date('from');
            $table->date('to')->nullable();
            $table->boolean('is_fixed')->default(1);
            $table->string('variable_part_description')->nullable();
            $table->integer('employee_id');
            
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salary_history');
    }
}
