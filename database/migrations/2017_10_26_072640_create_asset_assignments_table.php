<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_confirmed')->nullable();
            $table->date('deadline')->nullable();
            $table->date('assignment_date')->nullable();
            $table->date('return_date')->nullable();
            $table->string('request_message')->nullable();
            $table->string('return_reason')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('office_id')->nullable();
            $table->integer('asset_id')->nullable();
            $table->integer('confirmed_by')->nullable();
            
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_assignments');
    }
}
