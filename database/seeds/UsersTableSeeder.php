<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact1 = DB::table('contacts')->insertGetId([
            'first_name' => 'Mantas',
            'last_name' => 'Petrauskas',
            'created_by' => 1,
            'updated_by' => 1
        ]);

        $contact2 = DB::table('contacts')->insertGetId([
            'first_name' => 'Jonas',
            'last_name' => 'Jonaitis',
            'created_by' => 1,
            'updated_by' => 1
        ]);

        $contact3 = DB::table('contacts')->insertGetId([
            'first_name' => 'Petras',
            'last_name' => 'Petraitis',
            'created_by' => 1,
            'updated_by' => 1
        ]);

        DB::table('users')->insert([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'administrator',
            'contact_id' => $contact1,
            'created_by' => 1,
            'updated_by' => 1
        ]);

        DB::table('users')->insert([
            'email' => 'manager@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'manager',
            'contact_id' => $contact2,
            'created_by' => 1,
            'updated_by' => 1
        ]);

        DB::table('users')->insert([
            'email' => 'employee@gmail.com',
            'password' => bcrypt('password'),
            'role' => 'employee',
            'contact_id' => $contact3,
            'created_by' => 1,
            'updated_by' => 1
        ]);

    }
}
