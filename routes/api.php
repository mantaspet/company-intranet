<?php

use App\User;
use App\Contact;
use App\Employee;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

    Route::get('asset-assignments/requests', 'AssetAssignmentsController@getAssetRequests');
    Route::get('asset-assignments/my-requests', 'AssetAssignmentsController@getMyRequests');
    Route::get('asset-assignments/my-assets', 'AssetAssignmentsController@getMyAssets');
    Route::put('asset-assignments/return-asset', 'AssetAssignmentsController@returnAsset');
    Route::post('asset-assignments/new-request', 'AssetAssignmentsController@newRequest');
    Route::put('asset-assignments/update-request', 'AssetAssignmentsController@updateRequest');
    Route::apiResource('asset-assignments', 'AssetAssignmentsController');
    Route::get('assets/unassigned', 'AssetsController@getUnassignedAssets');
    Route::post('expenses', 'AssetsController@getExpenses');
    Route::apiResource('assets', 'AssetsController');
    Route::apiResource('contacts', 'ContactsController');
    Route::apiResource('departments', 'DepartmentsController');
    Route::apiResource('documents', 'DocumentsController');
    Route::get('my-employment-history', 'EmployeeHistoryController@getMyEmploymentHistory');
    Route::apiResource('employee-history', 'EmployeeHistoryController');
    Route::get('my-salary-history', 'EmployeeSalaryHistoryController@getMySalaryHistory');
    Route::apiResource('employee-salary', 'EmployeeSalaryHistoryController');
    Route::apiResource('employees', 'EmployeesController');
    Route::apiResource('employment-contracts', 'EmploymentContractsController');
    Route::get('my-holiday-history', 'HolidayHistoryController@getMyHolidayHistory');
    Route::get('employee-holiday/requests', 'HolidayHistoryController@requestIndex');
    Route::get('employee-holiday/my-requests', 'HolidayHistoryController@myHistory');
    Route::apiResource('employee-holiday', 'HolidayHistoryController');
    Route::apiResource('nti-application-requests', 'NtiApplicationRequestsController');
    Route::apiResource('offices', 'OfficesController');
    Route::apiResource('positions', 'PositionsController');
    Route::apiResource('salary-transaction-requests', 'SalaryTransactionRequestsController');
    Route::put('change-password', 'UsersController@changePassword');
    Route::put('suspend-user', 'UsersController@suspendUser');
    Route::put('reset-password', 'UsersController@resetPassword');
    Route::get('get-authenticated-user', 'UsersController@getAuthenticatedUser');
    Route::apiResource('users', 'UsersController');
    Route::get('oauth/current_user', 'UsersController@currentUser');
    
    
