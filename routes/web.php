<?php

use App\User;
use App\Contact;
use App\Employee;
use Illuminate\Http\Request;
use App\Events\OrderMaterials;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/api/employment-contracts/{id}/create-pdf/{url}', 'EmploymentContractsController@createPDF');
Route::get('/api/nti-application-requests/{id}/create-pdf/{url}', 'NtiApplicationRequestsController@createPDF');
Route::get('/api/salary-transaction-requests/{id}/create-pdf/{url}', 'SalaryTransactionRequestsController@createPDF');
