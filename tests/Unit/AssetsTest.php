<?php

namespace Tests\Unit;

use App\Asset;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AssetsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testCreateValidationErrorResponseAsAdmin()
    {
        $user = factory(User::class)->create(
            ['role' => 'administrator']
        );
//        $user = User::find(1);
        $response = $this->actingAs($user)->json('POST', 'api/assets', []);

        $response
            ->assertStatus(400)
            ->assertJson([
                'messages' => [],
            ]);
    }

    public function testCreateValidationErrorResponseAsNonAdmin()
    {
        $user = factory(User::class)->create(
            ['role' => 'manager']
        );
        $response = $this->actingAs($user)->json('POST', 'api/assets', []);


        $response
            ->assertStatus(403);
    }

    public function testCRUDSuccessResponse()
    {
        $user = factory(User::class)->create(
            ['role' => 'administrator']
        );

        $response = $this->actingAs($user)->json('POST', 'api/assets', [
            'name' => 'PC',
            'value' => 500,
            'acquisition_date' => '2017-11-02',
            'is_assigned' => 0,
            'type' => 'material'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'item' => [
                    'name' => 'PC',
                    'value' => 500,
                    'acquisition_date' => '2017-11-02',
                    'is_assigned' => 0,
                    'type' => 'material'],
            ]);
        $asset_id = json_decode($response->getContent())->item->id;

        $response = $this->json('PUT', 'api/assets/'.$asset_id, [
            'name' => 'PC1',
            'value' => 550,
            'acquisition_date' => '2016-10-03',
            'is_assigned' => 1,
            'type' => 'intellectual'
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'item' => [
                    'name' => 'PC1',
                    'value' => 550,
                    'acquisition_date' => '2016-10-03',
                    'is_assigned' => 1,
                    'type' => 'intellectual'],
            ]);

        $response = $this->json('DELETE', 'api/assets/'.$asset_id);

        $response
            ->assertStatus(200);
        User::destroy($user->id);

    }

//    public function testUpdateSuccessResponse()
//    {
//        $response = $this->json('PUT', 'api/assets/1', [
//            'name' => 'PC1',
//            'value' => 550,
//            'acquisition_date' => '2016-10-03',
//            'is_assigned' => 1,
//            'type' => 'intellectual'
//        ]);
//
//        $response
//            ->assertStatus(200)
//            ->assertJson([
//                'item' => [
//                    'name' => 'PC1',
//                    'value' => 550,
//                    'acquisition_date' => '2016-10-03',
//                    'is_assigned' => 1,
//                    'type' => 'intellectual'],
//            ]);
//    }
//
//    public function testDestroySuccessResponse()
//    {
//        $response = $this->json('DELETE', 'api/assets/1');
//
//        $response
//            ->assertStatus(200);
//    }
//
//    public function testIndex()
//    {
//        $response = $this->json('GET', 'api/assets');
//
//        $response
//            ->assertStatus(200)
//            ->assertJson([
//                'items' => [
//                    'data' => []
//                ],
//            ]);
//
//
//    }
}
